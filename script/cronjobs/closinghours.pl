#!/usr/bin/perl

## Sets users' times to opening hours' times.
## If the library closes at 6 pm, the users' time will be up at 6 pm.

## Runs constantly in the background

use strict;
use warnings;

use Env;
use Config::JFDI;
use DateTime::Format::MySQL;
use DateTime;

use FindBin;
use lib "$FindBin::Bin/../../lib";

use Libki::Schema::DB;

use Data::Dumper;

use POSIX qw(strftime);

my $config = Config::JFDI->new(
    file          => "$FindBin::Bin/../../libki_local.conf",
    no_06_warning => 1
);
my $config_hash  = $config->get();
my $connect_info = $config_hash->{'Model::DB'}->{'connect_info'};

my $schema = Libki::Schema::DB->connect($connect_info)
  || die("Couldn't Connect to DB");

my $idcounter = 0;
while ($schema) {
	my @sessions=$schema->resultset('Session')->all;

    if (@sessions){
        my $maxsessionid = $sessions[$#sessions]->id;

	    if($maxsessionid > $idcounter) {
	       	$idcounter++;
	       	closing();
	    }
    }
    sleep(1);
}

sub closing {  
	my @days = qw(sunday monday tuesday wednesday thursday friday saturday sunday);
	(my $sec,my $min,my $hour,my $mday,my $mon,my $year,my $wday,my $yday,my $isdst) = localtime(time);

	my $todaysdate = strftime "%F", localtime;
	my $currenthour = strftime "%k", localtime;
	my $currentminutes = strftime "%M", localtime;
	my $currenttime = ($currenthour*60)+$currentminutes;

	my $closinghours_rs = $schema->resultset('Closinghours');
	while ( my $closinghours = $closinghours_rs->next() ) {
    
		my $closinghour = substr $closinghours->closingtime, 0, 2;
		my $closingminute = substr $closinghours->closingtime, 3,-3;
		my $closingtime = ($closinghour*60)+$closingminute;
		my $timedifference = $closingtime - $currenttime;
    
		if ($closinghours->day eq $todaysdate ) {

			if ($timedifference < $schema->resultset('Setting')->find('DefaultTimeAllowance')->value) {
				my $activeuser_rs = $schema->resultset('Session');
				
				while ( my $activeuser = $activeuser_rs->next() ) {
					my $activeuserminutes = $activeuser->user->minutes();
										
					if ( $activeuser->user->minutes() > $timedifference ) {
						my $decreaseminutes = $activeuserminutes - $timedifference;
						$activeuser->user->decrease_minutes($decreaseminutes);
						$activeuser->user->update();
					}
				}
			}
			last;
		}
		elsif ($closinghours->day eq $days[$wday] ) {
				
			if ($timedifference < $schema->resultset('Setting')->find('DefaultTimeAllowance')->value) {
				
				my $activeuser_rs = $schema->resultset('Session');
				
				while ( my $activeuser = $activeuser_rs->next() ) {
					my $activeuserminutes = $activeuser->user->minutes();
					
					if ( $activeuser->user->minutes() > $timedifference ) {
						my $decreaseminutes = $activeuserminutes - $timedifference;
						$activeuser->user->decrease_minutes($decreaseminutes);
						$activeuser->user->update();
					}
				}
			}
			last;
		}
	}
}
